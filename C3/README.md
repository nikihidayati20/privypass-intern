# Cashier POS
Cashier POS is a web application that provides a Point of Sale system that integrates with your transactions, inventory and employee management.

1. Login as superadmin
- Register your employee as admin or cashier
- Edit your employee's role (admin or cashier)
- Monitor your daily, weekly, and monthly incomes
- Monitor your transaction 
- Monitor your product's supply and demand
2. Login as admin
- Manage your product stock
- Monitor your product's supply and demand
3. Login as cashier
- Record transactions with barcode
- Make a receipt of new transaction

## Usecase [(Whimsical link)](https://whimsical.com/c3-uml-DMUUNVGdbqz5RfZETffhxS@2Ux7TurymN8Nb6n1Xjyd)
![Usecase diagram](./img/C3%20-%20Usecase.png)

## Kanban Management Tool [(Figma link)](https://alpine-orchestra-107.notion.site/2c115f3092a84882aa0ba1d012036a28?v=af29c5c7028d436b99b81215cea39319)

## ERD [(Whimsical link)](https://whimsical.com/c3-erd-3Sab7GD4DziZ3HckJCGUoR@7YNFXnKbYnrQvgDbqBT8g)
![ERD](./img/C3%20-%20ERD.png)

## Relation Schema
![Relational Schema](./img/C3%20-%20Relational%20Schema.png)

## API Contract [(Whimsical link)](https://whimsical.com/c3-api-contract-FY18LP8GpDAJFUwopwFTNu@7YNFXnKbYixwmTgNE8eyA)

## Dashboard Management [(Retool link)](https://niki20.retool.com/apps/968740c8-8b4a-11ed-9943-c3b5698ed968/C3%20Cashier%20System)

## UI Design [(Figma link)](https://www.figma.com/file/xibg5hDB2baq8lcq7ktMdB/C3---personal-project?node-id=18%3A103&t=qemffkIXSoAARF16-1)

![](./img/Desktop%20-%201.png)
![](./img/Desktop%20-%202.png)
![](./img/Desktop%20-%203.png)
![](./img/Desktop%20-%204.png)