import {ref} from 'vue'
import axios from 'axios'

export default function useProduct() {
    const url = "http://127.0.0.1:8081/products"
    const productData = ref([])
    const error = ref(null)

    // get all products data
    const getAllProduct = async () => {
        productData.value = []
        error.value = null
        // try {
        const res = await axios.get(url).then(res=> {
            // console.log("[res] ", res.data.data)
            productData.value = res.data.data
            res.data.data
        }).catch(err => {
            console.error('terjadi error untuk mendapatkan product', err);
        })
        // console.log("[log res]", productData)
        return productData.value
            
            // studentData.value = res.data
            // console.log(res.data)
        // } catch (err) {
        //     error.value(err)
        //     console.log(err)
        // }
    }

    return {
        productData,
        error,
        getAllProduct
    }
}