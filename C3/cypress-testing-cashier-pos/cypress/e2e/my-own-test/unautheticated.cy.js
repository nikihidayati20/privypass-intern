describe("basic unautheticated desktop test", () => {
    beforeEach(() => {
        cy.viewport(1280, 720)
        cy.visit('https://codedamn.com/')
    })

    it("login page looks good", () => {
        cy.contains("Sign In").click()
        cy.contains("Sign in to").should("exist")
        cy.contains("codedamn").should("exist")
        cy.contains("Create Account").should("exist")
        cy.contains("Forgot your password?").should("exist")
    })
    
    it("login page links work", () => {
        cy.contains("Sign In").click()
        cy.url().should('include', '/login')
        cy.go('back')

        cy.url().then((value) => {
            cy.log("the current URL is ", value)
        })
        // cy.contains('Forgot your password?').click()
    })

    it('login shoud display correct error', () => {
        cy.contains("Sign In").click()
        cy.url().should('include', '/login')
        cy.contains("Unable to authorize. Please check username/password combination").should('not.exist')
        
        cy.get('[data-testid=username]').focus().type('nikih')
        cy.get('[data-testid=password]').focus().type('niki')

        
        cy.get('[data-testid="login"]').click({force:true})

        cy.contains("Unable to authorize. Please check username/password combination").should('exist')

    })
})