# Retool
project link: [https://niki20.retool.com/apps/86ac0d1a-836f-11ed-b29d-f3b629bf49e4/privypass%20intern%20task](https://niki20.retool.com/apps/86ac0d1a-836f-11ed-b29d-f3b629bf49e4/privypass%20intern%20task)
![retool screenhot](./images/retool.png)

# Gitlab Ticketing
![gitlab issue](./images/gitlab%20tickets%20-%20issue.png)
![gitlab list](./images/gitlab%20tickets%20-%20list.png)
![gitlab board](./images/gitlab%20tickets%20-%20boards.png)
