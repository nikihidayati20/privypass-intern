# Converting ER Diagram into Table
project link: [here](https://whimsical.com/privypass-intern-erd-to-table-QjkL46QoAN1M78N5fpbjN9@7YNFXnKbYnmQDTKWsoCpa)
![ERD to table](./Convert%20ERD%20to%20Table/screenshot/ERD%20to%20table%20-%20rule%201.jpg)
![ERD to table](./Convert%20ERD%20to%20Table/screenshot/ERD%20to%20table%20-%20rule%202.jpg)
![ERD to table](./Convert%20ERD%20to%20Table/screenshot/ERD%20to%20table%20-%20rule%203.jpg)
![ERD to table](./Convert%20ERD%20to%20Table/screenshot/ERD%20to%20table%20-%20rule%204.jpg)
![ERD to table](./Convert%20ERD%20to%20Table/screenshot/ERD%20to%20table%20-%20rule%205.jpg)
![ERD to table](./Convert%20ERD%20to%20Table/screenshot/ERD%20to%20table%20-%20rule%206.jpg)
![ERD to table](./Convert%20ERD%20to%20Table/screenshot/ERD%20to%20table%20-%20rule%207.jpg)


# Supabase
- Supabase table
![Supabase](./SupaBase/screenshot/supabase%20-%20table.jpg)
- Supabase authentication
![Supabase](./SupaBase/screenshot/supabase%20-%20auth.jpg)
- Supabase policies
![Supabase](./SupaBase/screenshot/supabase%20-%20policies.jpg)
- Supabase API call
![Supabase](./SupaBase/screenshot/supabase%20-%20api%20get%20rows.jpg)

# Supabase dan Vue js
*Screenshot demo:*
![Vue website demo](./SupaBase%20and%20Vue3/screenshot/vue%20-%20register%20error%20validation.jpg)
![Vue website demo](./SupaBase%20and%20Vue3/screenshot/vue%20-%20login%20error%20validation.jpg)
![Vue website demo](./SupaBase%20and%20Vue3/screenshot/vue%20-%20add%20new%20data.jpg)
![Vue website demo](./SupaBase%20and%20Vue3/screenshot/vue%20-%20home.jpg)
![Vue website demo](./SupaBase%20and%20Vue3/screenshot/vue%20-%20home%20(detailed%20exercise).jpg)
![Vue website db](./SupaBase%20and%20Vue3/screenshot/vue%20-%20db%20auth.jpg)
![Vue website db](./SupaBase%20and%20Vue3/screenshot/vue%20-%20db%20workouts%20table.jpg)

# User Story
project link: [here](https://alpine-orchestra-107.notion.site/Privypass-Intern-6b34bc05663c4b4d8b1ab789cde73cc7)
![User story & tasks](./User%20story%20%26%20tasks/user%20story%20%26%20tasks.png)