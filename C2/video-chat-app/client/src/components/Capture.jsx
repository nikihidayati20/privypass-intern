import React from 'react';
import {render} from 'react-dom';
import html2canvas from "html2canvas"

const Capture = () => {
    const snap = () => {
        html2canvas(document.body).then(function (canvas) {
            var a = document.createElement('a')
            a.href = canvas.toDataURL("...assets/image/jpg").replace("image/jpg", "image/octet-system")
            a.download = `screenshot.jpg`
            a.click()
        })
    }
    return (<button onClick={snap}>Screenshot</button>);

}

export default Capture;
