# Postman Mockup
- mock server 1
![mock server 1 screenshot](./images/mock%20server%201%20-%201.jpg)
![mock server 1 screenshot](./images/example%20mock%20server%201%20-%201.jpg)
![mock server 1 screenshot](./images/mock%20server%201%20-%202.jpg)
![mock server 1 screenshot](./images/example%20mock%20server%201%20-%202.jpg)
- mock server 2
![mock server 2 screenshot](./images/mock%20server%202.jpg)
![mock server 2 screenshot](./images/example%20mock%20server%202%20-1%20.jpg)
![mock server 2 screenshot](./images/example%20mock%20server%202%20-2.jpg)

# Task Based UI
1. Focus each page on one main task
2. Make your tasks be like commands
3. Be mindful of your model
4. Check your links
5. Follow established UI pattern

# User Flow Diagram
![userflow diagram](./images/userflow%20diagram.jpg)
common symbols used in userflow:
- Oval — start or endpoint
- Rectangle — process or action
- Parallelogram — input or output
- Diamond — decision
- Arrow — direction

contoh userflow:
![userflow diagram example](./images/userflow%20diagram%20example.jpg)