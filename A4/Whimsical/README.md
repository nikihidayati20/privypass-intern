# Whimsical Wireframes
1. [Frames](#frames)
2. [Elements](#elements)
3. [Shortcuts](#shortcuts)
4. [Wireflow](#wireflow)

Go to my whimsical board [here (bit.ly/whimsical-privyintern-task)](https://bit.ly/whimsical-privyintern-task)

# Frames
![frames](./images/frames%20-%20choose%20frames.jpg)
Main Features:
![frames - features](./images/frames%20-%20features.jpg)
- Rename frame
- Show/hide status bar
- Show/hide keyboard
- Change orientation
- Change frames's background color
- Duplicate frame
- Add comment to frame

# Elements
![elements](./images/elements.jpg)
- Text
![elements](./images/elements%20-%20text.jpg)
- Button, Link, and Tab
![elements](./images/elements%20-%20button%2C%20links%2C%20tabs.jpg)
- Forms
![elements](./images/elements%20-%20forms.jpg)
- Shapes
![elements](./images/elements%20-%20shapes.jpg)
- Switches & Misc
![elements](./images/elements%20-%20switches%20n%20misc.jpg)

# Shortcuts
### General Shortcuts
| Keyboard Shortcuts (For PC) | Definition         |
| --------------------------- | ------------------ |
| Copy Style  | `Control + Alt + C`  |
| Duplicate  | `Control + D / Alt + Drag`  |
| Bring Forward  | `]` |
| Bring to Front  | `Alt + ]` |
| Group Selection  | `Control + G` |
| Ungroup Selection  | `Control + Shift + G` |
| Deep Select  | `Control + Click` |
| Ignore Auto-Snapping When Moving Object  | `Control + Drag` |
| Ignore Grid & Auto-Snapping When Moving Object  | `` + Drag` |
| Save as Default Style  | `Control + Shift + D` |
| Comment | `Control + Alt + M` |
| Show/Hide comments in a file | `Shift + Alt + C` |
| Animate Connector | `Control + Click` |
| Get Shareable Link | `Control + Alt + S` |
| Change Board Mode | `W + B` (Board), `W + W` (Wireframe), `W + P` (Project) |
More shortcuts: [here](https://help.whimsical.com/article/622-keyboard-shortcuts#board-pc)

# WireFlow
![wireflow](./images/wireflow.jpg)