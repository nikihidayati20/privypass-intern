# Sequential Diagram
![sequential diagram](./screenshot/Sequential%20Diagram.jpg)

# Get Access Token
![get access token](./screenshot/Get%20Access%20Token.jpg)

# Get Identity
![get identity](./screenshot/Get%20Identity.jpg)