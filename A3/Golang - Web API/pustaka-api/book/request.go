package book

import "encoding/json"

// struct untuk menangkap data dari client
type CreateBookRequest struct {
	Title       string      `json:"title" binding:"required"`
	Price       json.Number `json:"price" binding:"required,number"`
	Description string      `json:"description" binding:"required"`
	Rating      json.Number `json:"rating" binding:"required,number"`
	// SubTitle string      `json:"sub_title"`
}
type UpdateBookRequest struct {
	Title       string      `json:"title"`
	Price       json.Number `json:"price"`
	Description string      `json:"description"`
	Rating      json.Number `json:"rating"`
	// SubTitle string      `json:"sub_title"`
}
