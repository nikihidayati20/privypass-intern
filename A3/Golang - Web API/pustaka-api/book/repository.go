package book

import "gorm.io/gorm"

// interface
type Repository interface {
	// function apa saja yg akan dipakai pada table book
	// function() (return type)
	FindAll() ([]Book, error)
	FindByID(ID int) (Book, error)
	// FindByTitle(title string) (Book, error)
	Create(book Book) (Book, error)
	Update(book Book) (Book, error)
	Delete(book Book) (Book, error)
}

// repository implementation
type repository struct {
	// var utk mengakses db
	db *gorm.DB
}

// fungsi untuk menginisiasi struct
func NewRepository(db *gorm.DB) *repository {
	return &repository{db}
}

func (r *repository) FindAll() ([]Book, error) {
	var books []Book
	err := r.db.Find(&books).Error
	return books, err
}
func (r *repository) FindByID(ID int) (Book, error) {
	var book Book
	err := r.db.Find(&book, ID).Error
	return book, err
}

// func (r *repository) FindByTitle(title string) ([]Book, error) {
// 	var books []Book
// 	err := r.db.Where("Title = ?", title).Find(&books).Error
// 	return books, err
// }
func (r *repository) Create(book Book) (Book, error) {
	err := r.db.Create(&book).Error
	return book, err
}

func (r *repository) Update(book Book) (Book, error) {
	err := r.db.Save(&book).Error
	return book, err
}

func (r *repository) Delete(book Book) (Book, error) {
	err := r.db.Delete(&book).Error
	return book, err
}
