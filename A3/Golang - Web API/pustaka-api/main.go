package main

import (
	"fmt"
	"log"
	"pustaka-api/book"
	"pustaka-api/handler"

	"github.com/gin-gonic/gin"
	"gorm.io/driver/mysql"
	"gorm.io/gorm"
)

func main() {
	dsn := "root:@tcp(127.0.0.1:3306)/db_pustaka_api?charset=utf8mb4&parseTime=True&loc=Local"
	db, err := gorm.Open(mysql.Open(dsn), &gorm.Config{})

	if err != nil {
		log.Fatal("Db connection error")
	}

	// buat tabel
	db.AutoMigrate(&book.Book{})
	fmt.Println("db connection succeeded")

	bookRepository := book.NewRepository(db)
	bookService := book.NewService(bookRepository)
	bookHandler := handler.NewBookHandler(bookService)

	// books, err := bookRepository.FindAll()
	// for _, book := range books {
	// 	fmt.Println("Title: ", book.Title)
	// }

	// book, err := bookRepository.FindByID(2)
	// fmt.Println("Title: ", book.Title)

	// newBook := book.BookRequest{}
	// newBook.Title = "$100 Startup"
	// newBook.Price = "95000"
	// bookService.Create(newBook)

	router := gin.Default()

	// versioning
	v1 := router.Group("/v1")

	// routing
	v1.GET("/books", bookHandler.GetBooks)
	v1.GET("/books/:id", bookHandler.GetBook)
	v1.GET("/query", bookHandler.QueryHandler)
	v1.POST("/books", bookHandler.CreateBook)
	v1.PUT("/books/:id", bookHandler.UpdateBook)
	v1.DELETE("/books/:id", bookHandler.DeleteBook)

	router.Run() // default port (8080)
	// router.Run(":8888") // custom port
}
