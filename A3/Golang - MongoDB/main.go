package main

import (
	"golang-mongodb/controllers"
	"log"
	"net/http"

	"github.com/julienschmidt/httprouter"
	"gopkg.in/mgo.v2"
)

func main() {
	r := httprouter.New()
	// session
	uc := controllers.NewUserController(getSession())

	r.GET("/user/:id", uc.GetUser)
	r.POST("/user", uc.CreateUser)
	r.DELETE("/user/:id", uc.DeleteUser)
	http.ListenAndServe("localhost:8000", r)
}

func getSession() *mgo.Session {
	s, err := mgo.Dial("localhost:27017")
	if err != nil {
		log.Panic("Error to connect database")
	}
	return s
}
