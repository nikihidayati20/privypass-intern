package main

import (
	"booking-app/helper"
	"fmt"
	"sync"
	"time"
)

// package level variables
// cannot declare variables using sintatic sugar

// sintactic sugar in Go
// hanya untuk var (bkn const), dan tidak untuk jjika ngin define typenya secara eksplisit
// conferenceName := "Go Conference"
var conferenceName = "Go Conference"

const conferenceTickets = 50

var remainingTickets uint = 50

// MULTIVALUE DATA TYPE
// array
// saat mendefine nya butuh size! karna arraymmeiliki fixed size.
// semua data type darielemen array harus sama
// e.g. var bookings = [arraySize]arrayType{initialArrayValue}
// var bookings [50]string
// bookings[0] = fullName

// list
// doon't need to spesify the size

// slice
// an abstraction of an array
// has array's behavior but with dinamyc size
// var bookings = make([]map[string]string, 0) // slice of map
var bookings = make([]UserData, 0) // slice of struct
// struct
type UserData struct {
	firstName       string
	lastName        string
	email           string
	numberOfTickets uint
	// isOptedInForNewsletter bool
}

// WaitGroup --> waits for the launched goroutine to finish
var wg = sync.WaitGroup{}

func main() {
	greetUsers()

	// LOOP
	//
	// for { // INFINITE LOOP
	// }
	//
	// for remainingTickets > 0 && len(bookings) < 50 { // END OF A LOO
	// }

	for { // INFINITE LOOP
		firstName, lastName, email, userTickets := getUserInputs()
		fmt.Println("=======================================")

		isValidName, isValidEmail, isValidUserTicketNumber := helper.ValidateUserInputs(firstName, lastName, email, userTickets, remainingTickets)

		if isValidName && isValidEmail && isValidUserTicketNumber {
			bookTicket(userTickets, firstName, lastName, email)
			// WaitGroup
			// wg.Add(1) // berapa thread yang mau ditambahkan dan ditunggu
			//  Goroutines
			go sendTicket(userTickets, firstName, lastName, email)

			firstNames := getBookingsData()

			fmt.Println("")
			fmt.Printf("These are all our bookings: %v\n", firstNames)
			fmt.Println("See you onthe conference! ^^")
			fmt.Println("==============================================")

			// CONDITIONAL
			if remainingTickets == 0 {
				// end program
				fmt.Println("Our conference is booked out. Come back next year.")
				break
			}
		} else {
			if !isValidName {
				fmt.Println("first name or last name you entered is too short")
			}
			if !isValidEmail {
				fmt.Println("email address you entered doesn't contain @ sign")
			}
			if !isValidUserTicketNumber {
				if userTickets < 0 {
					fmt.Println("number of tickets you entered is invalid")
				} else {
					fmt.Printf("Unfortunately, we only have %v tickets remaining,so tou can't book %v tickets\n", remainingTickets, userTickets)
				}
			}
			// WaitGroup
			// wg.Wait()
		}

	}

}

func greetUsers() {
	fmt.Printf("Welcome to %v booking application1\n", conferenceName)
	fmt.Printf("We have total of %v tickets and %v are still awailable.\n", conferenceTickets, remainingTickets)
	fmt.Println("Get your tickets here to attend")
}

// func functionName(parameter parameterType, parameter paramaterType) returnType {}
func getBookingsData() []string {
	firstNames := []string{}
	// iterating element of a map
	// _ means: blank identifier. to ignore variable we don't use
	for _, booking := range bookings {
		// strings.Fields() --> split a string
		// var names = strings.Fields(booking)
		firstNames = append(firstNames, booking.firstName)
		fmt.Printf("List of bookings is %v\n", bookings)
	}
	return firstNames
}

func getUserInputs() (string, string, string, uint) {
	var firstName string
	var lastName string
	var email string
	var userTickets uint // uint typedoesn'tallow negative value

	// pointer --> memory address yang menyimpan suatu variabel itu
	// fmt.Println(&fullName)

	// get in input from uer
	println("")
	println("------------------------------------")
	fmt.Print("Enter your firstname: ")
	fmt.Scan(&firstName) // simpan input dari user ke dalam memori address dari suatu var

	fmt.Print("Enter your lastname: ")
	fmt.Scan(&lastName) // simpan input dari user ke dalam memori address dari suatu var

	fmt.Print("Enter your email: ")
	fmt.Scan(&email)

	fmt.Print("Enter number of ticktets: ")
	fmt.Scan(&userTickets)

	return firstName, lastName, email, userTickets
}

func bookTicket(userTickets uint, firstName string, lastName string, email string) {
	remainingTickets = remainingTickets - userTickets

	// create a map
	var userData = UserData{
		firstName:       firstName,
		lastName:        lastName,
		email:           email,
		numberOfTickets: userTickets,
	}

	bookings = append(bookings, userData)

	fmt.Println("")
	fmt.Printf("Thank you %v %vfor booking %v tckets. Youu will recieve a confirmation email at %v\n", firstName, lastName, userTickets, email)
	fmt.Printf("%v tickets remaining for %v\n", remainingTickets, conferenceName)
}

func sendTicket(userTicket uint, firstName string, lastName string, email string) {
	time.Sleep(10 * time.Second)
	var ticket = fmt.Sprintf("%v tickets for %v %v", userTicket, firstName, email)
	fmt.Println("\n#######################")
	fmt.Printf("Sending ticket:\n %v \nto email address %v\n", ticket, email)
	fmt.Println("#######################")

	// WaitGroup
	// remove the tread yang sebelumnya di add
	// wg.Done()
}
