package helper

import "strings"

// global variable
// can be  used everywhere across all packages
// declaration:  uppercase first letter
var GlobalVar = "this is a global variable"

// capitalize first letter => means we are exporting these function so that it can be imported from another package
func ValidateUserInputs(firstName string, lastName string, email string, userTickets uint, remainingTickets uint) (bool, bool, bool) {
	isValidName := len(firstName) >= 2 && len(lastName) >= 2
	isValidEmail := strings.Contains(email, "@")
	isValidUserTicketNumber := userTickets > 0 && userTickets <= remainingTickets

	// in Go we can return multiple values
	return isValidName, isValidEmail, isValidUserTicketNumber
}
