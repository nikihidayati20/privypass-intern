# Chrome Dev Tools

1. [Elements](#elements)
2. [Sources](#sources)
3. [Network](#network)
5. [Performance](#performance)
8. [Memory](#memory)
6. [Application](#application)
7. [Security](#security)
9. [Sensors](#sensors)
9. [WebAuthn: Emulate authenticators](#webAuthn:-emulate-authenticators)

# Elements

## viewing and changing the DOM
![elements - changing the DOM](./images/elements%20-%20DOM.jpg)
## View the properties of DOM objects
![elements - DOM properties](./images/elements%20-%20DOM%20properties.jpg)
## Badges reference
![elements - Badges reference](./images/elements%20-%20Badges%20reference.jpg)
## View and change CSS
![elements - css](./images/elements%20-%20css.jpg)
## Find invalid, overridden, inactive, and other CSS
![elements - invalid css](./images/elements%20-%20invalid%20css.jpg)
![elements - overriden css](./images/elements%20-%20css%20overriden.jpg)
## Features References
![elements - Features References](./images/elements%20-%20css%20features%20reference.jpg)
## logged messages & running JavaScript
![elements](./images/console%20-%20logged%20messages%20%26%20running%20JS.jpg)
## format specifier
![elements](./images/console%20-%20format%20specifier.jpg)
## console sidebaar
![elements](./images/console%20-%20sidebar.jpg)

# Sources
## Panel UI
![elements](./images/sources%20-%20panel%20UI.jpg)
1. File navigator
2. Code editor
3. JavaScript Debugging
## Breakpoints
![elements](./images/console%20-%20breakpoints.jpg)
## Console snippet
![elements](./images/console%20-%20snippet.jpg)

# Network
## Log Activity
![elements](./images/network%20-%20log%20activity.jpg)
## Throttling 
![elements](./images/network%20-%20Throttling%20.jpg)
## Show More Information
![elements](./images/neetwork%20-%20more%20info.jpg)
## Captures Screenshots
![elements](./images/network%20-%20screenshots.jpg)
## Inspect a resource's details (Header, Preview, Response, Initiator, Timing)
![elements](./images/network%20-%20details%20header.jpg)
![elements](./images/network%20-%20details%20preview.jpg)
![elements](./images/network%20-%20details%20response.jpg)
![elements](./images/network%20-%20details%20initator.jpg)
![elements](./images/network%20-%20details%20timing.jpg)
## Search network headers and responses
![elements](./images/network%20-%20search.jpg)
## Filter resources
![elements](./images/network%20-%20filter%20resources.jpg)
## Block requests
![elements](./images/network%20-%20block%20-%201.jpg)
![elements](./images/network%20-%20block%20-%202.jpg)

# Performance
## Analyze runtime performance
![elements](./images/performance%20-%20runtime%20performance.jpg)
## Analyze frames per second
![elements](./images/performances%20-%20fps.jpg)
## Find the bottleneck
![elements](./images/performances%20-bottleneck.jpg)
## Simulate a mobile CPU
![elements](./images/performance%20-simulate%20cpu.jpg)
## Record runtime performance
![elements](./images/performance%20-%20Record%20runtime%20performance.jpg)

# Memory
## Heap snapshots
![elements](./images/memory%20-%20heap%20snapshot.jpg)
## Allocation Profiler Tool
![elements](./images/memory%20-%20heap%20snapshot.jpg)

# Application
## Debug Progressive Web Apps
## Allocation Profiler ToolView and edit local storage
![elements](./images/application%20-%20local%20storage.jpg)
## Allocation Profiler ToolView and edit local storage
![elements](./images/application%20-%20local%20storage.jpg)
## Session storage
![elements](./images/application%20-%20session%20storage.jpg)
## View and change IndexedDB data
![elements](./images/application%20-%20indexDB%20data.jpg)
## Cache
![elements](./images/application%20-%20cache.jpg)

# Security
![elements](./images/security.jpg)

# Sensors
## Override geolocation
![elements](./images/sensors%20-%20geolocation.jpg)
## Simulate device orientation
![elements](./images/sensors%20-%20orientation.jpg)

# WebAuthn: Emulate authenticators
![elements](./images/web%20authn.jpg)