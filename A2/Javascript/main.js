// variable 
const yearOfBirth = 2025;
let age = 15;
 
console.log(yearOfBirth, age);

// data type:  Stirng, Number, boolean, null, undefined
const nama = "niki";
const favNum = 1;
const rating = 4.5;
const isAPerson = true;
const x = null;
const y = undefined;
let z;

console.log(typeof nama, typeof favNum, typeof rating, typeof isAPerson, typeof x, typeof y, typeof z);

// Stirng concanetation & template string
console.log("hi " + nama);
console.log(`hi ${nama}`);

// Stirng property
const s = 'Hello All!';
console.log(s.length);
console.log(s.toLowerCase);
console.log(s.toUpperCase);
console.log(s.substring(0,  5), s.substring(s.length-1));
console.log(s.split(''));

// array
const wargaKampungDurianRuntuh = new Array('Upin', 'Ipin', 'Fizi', 'Atok Dalang', true, 11);
const wargaKampungDurianRuntuh2 = ['Ehsan', 'Susanti', 'Kak Ros', false, 0];
console.log(Array.isArray(wargaKampungDurianRuntuh));
console.log(Array.isArray(wargaKampungDurianRuntuh2));
console.log(wargaKampungDurianRuntuh.indexOf('Fizi'))

console.log(wargaKampungDurianRuntuh[wargaKampungDurianRuntuh.length - 1]);
console.log(wargaKampungDurianRuntuh2[0]);

wargaKampungDurianRuntuh.push('Mail');
console.log(wargaKampungDurianRuntuh[wargaKampungDurianRuntuh.length - 1]);
wargaKampungDurianRuntuh.pop();
console.log(wargaKampungDurianRuntuh[wargaKampungDurianRuntuh.length - 1]);

wargaKampungDurianRuntuh2.unshift('Jarjit');
console.log(wargaKampungDurianRuntuh2[0]);

// object literal
const person = {
    firstname:  'Niki',
    age: 19,
    hobbies: ['music', 'running'],
    address: {
        city: 'yogyakarta',
        state: 'indonesia'
    }
}
console.log('hobby:', person.hobbies[1] + ';', 'state:' , person.address.state);

const {firstname, address: {city}} = person;
console.log(firstname, city);

person.email = 'niki@mail.com';
console.log(person.email);
console.log(person);

// array of an object
const todos = [
    {
        id: '1',
        text: 'take out trash',
        isComplete: true
    },
    {
        id: '2',
        text: 'meeting with boss',
        isComplete: true
    },
    {
        id: '3',
        text: 'dentist appt',
        isComplete: false
    }
]

console.log("unclomplete todo: ", todos[2].text);

// // convert array to json
// const todoJSON  = JSON.stringify(todos);
// console.log(todoJSON);

// // for loops
// console.log('--- for loop ---')
// for(let i = 0; i < todos.length; i++) {
//     console.log(`todo number ${i}: ${todos[i].text}`)
// }

// // while loop
// let i = 0;
// console.log('--- while loop ---')
// while (i < todos.length) {
//     console.log(`todo number ${i}: ${todos[i].text}`)
//     i++
// }

// // for each, map, filter
// console.log('--- for each loop ---');
// todos.forEach(function(todo)  {
//     console.log(todo.text)
// });

// console.log('--- map wil return an array ---');
// const todoText = todos.map(function(todo)  {
//     return todo.text;
// });
// console.log(todoText);

// console.log('--- only show completed todos with filter ---');
// const todoCompleted = todos.filter(function (todo) {
//     return todo.isComplete ===true;
// }).map(function (todo) {
//     return todo.text;
// })
// console.log(todoCompleted);

// // conditional
// // == doesn't compare the type, only it's value
// /// === comparing the value and the type
// a = 10;

// if (a === 10) {
//     console.log('the value of a is 10');
// } else if (x > 10) {
//     console.log('a is greater then 10');
// } else {
//     console.log('a is less than 10');
// }

// // logical operator
// // || OR
// // && AND
// // ! NOT

// // ternary operator
// const n = 10;
// const color = n > 5 ? 'blue' : 'red';


// // switch case
// switch (color) {
//     case 'red':
//         console.log('color is red');
//         break;
//     case 'blue':
//         console.log('color is blue');
//         break;
//     default:
//         console.log('color is neither red nor blue');
//         break;
// }

// // function with default value
// function addNums(num1 = 0, num2 = 0) {
//     return num1 + num2;
// }
// console.log(addNums(10));

// // arrow function
// const subNums = (num1, num2) => num1 -num2;
// console.log(subNums(6, 5));;

// // object oriented programming in ES5
// // function Person(name, dob) { // constructor
// //     this.name = name; 
// //     this.dob = new Date(dob);
// // }

// // Person.prototype.getBirthYear= function () { // methods
// //     return this.dob.getFullYear();
// // }

// // const person1= new Person("Niki",  '1-1-2000');
// // console.log(person1.name);
// // console.log(person1.getBirthYear());

// // object oriented programming in ES66
// class Person {
//     constructor(name, dob) {
//         this.name = name;
//         this.dob = new Date(dob);
//     }

//     getBirthYear= function () {
//         return this.dob.getFullYear();
//     }
// }

// const person2= new Person("Niki",  '1-1-2000');
// console.log(person2.name);
// console.log(person2.getBirthYear());

// // DOM
// console.log(window) // window => parent object of the browser

// // selector single element
// document.getElementById('my-form');
// document.querySelector('h1');
 
// // selector single element
// console.log(document.querySelectorAll('.item'));

// // DOM maniputation
// const ul = document.querySelector('.items');
// // ul.remove(); //remove an element
// // ul.lastElementChild.r emove(); // remove only last child
// ul.firstElementChild.textContent = ul.children[0].textContent + " hello!";
// ul.children[1].innerText = 'HI!'
// ul.lastElementChild.innerHTML = '<h4>hola</h4>'

// const btn = document.querySelector('.btn');
// btn.style.background = 'blue';
// btn.addEventListener('click', (e) => {
//     e.preventDefault();
//     console.log(e);
//     console.log(e.target);
//     btn.style.background = '#ccc';
//     btn.style.color = '#000';
// });

const myForm = document.querySelector('#my-form');
const inName = document.querySelector('#name');
const inEmail = document.querySelector('#email');
const msg = document.querySelector('.msg');
const userList = document.querySelector('#users');

myForm.addEventListener('submit', onSubmit);
function onSubmit(e){
    e.preventDefault();

    if (inName.value ===  '' || inEmail.value === '') {
        msg.classList.add('error');
        msg.innerText = 'Please enter all fields';

        setTimeout(() => msg.remove(), 3000);
    } else {
        const li = document.createElement('li');
        li.appendChild(document.createTextNode(`${inName.value} : ${inEmail.value}`));
     userList.appendChild(li);

        // clear fields
        inName.value = '';
        inEmail.value = '';
    }
}